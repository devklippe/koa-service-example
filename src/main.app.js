const Koa = require('koa');
const app = new Koa();

app.use(async (cont, next) => {
	cont.body = 'Service example!';
});

module.exports = app;