const logger = require('intel');

logger.basicConfig({
    'format':'<%(name)s>[%(date)s] ==> %(message)s'
});

module.exports = logger;