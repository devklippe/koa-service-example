const app = require('./src/main.app');
const cluster = require('cluster');
const logger = require('./src/service.configs/logger.config');
const cpu_count = require('os').cpus().length;
/*

 */
if (cluster.isMaster){
	for (let i = 0; i < cpu_count; i++) {
		cluster.fork();
	}
	
	cluster.on('exit', () => {
		logger.info(`worker ${process.pid} died`);
	});
} else {
	app.listen(3000, ()=>{
		logger.info(`Service up on port 3000 cluster: ${process.pid}`);
	});
}