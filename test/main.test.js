const assert = require('assert');
const main = require('../src/functions/main.function');
describe('Example of test!', () => {
	it('Return message!', async () => {
		assert.equal('Hellow word', await main('Hellow word'));
	});
});